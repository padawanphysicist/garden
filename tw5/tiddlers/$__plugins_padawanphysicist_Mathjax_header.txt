<script type="text/javascript">
MathJax = {
  tex: {
    inlineMath: [['$', '$'], ['\\(', '\\)']]
  }
};
setInterval(function() {
  MathJax.typesetClear(); 
  MathJax.texReset();                 
  MathJax.typeset();        
  // console.log('Render Math');          
}, 5000);
</script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>